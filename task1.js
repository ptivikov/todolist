/**
 * Задание 1
 *
 * 1. Реализуем приложение Список дел с возможностью добавить новый элемент в список из инпута по нажатию кнопки "Добавить";
 * 2. При нажатии на галочку текст итема становится зачеркнутым, при отжатии снова обычным.
 * 3. Реализовать удаление элемента по нажатию крестика;
 * 4. Реализовать возможность редактирования элемента:
 * - При нажатии на текст элемента он отображается в инпуте, название кнопки меняется на "Обновить";
 * - После изменения названия и нажатия на "Обновить" текст элемента изменяется в списке, инпут сбрасывается, кнопка переименовывается обратно в "Добавить".;
 * - Предусмотреть возможность отменить редактирование (крестик в конце инпута), при нажатии на который редактирование прекращается: инпут очищается, кнопка вновь имеет заголовок "Добавить".
 * - Редактируемый элемент подсвечивается как активный.
 * 5*. Сохрание текущего состояния (local storage).
 */

const btnAddEl = document.querySelector('.js-btn-add');
const btnClearEl = document.querySelector('.js-btn-clear');
const btnUpdate = document.querySelector('.js-btn-update');
const btnBackspace = document.querySelector('.js-btn-backspace');
const inputEl = document.querySelector('.js-input');

document.addEventListener("DOMContentLoaded", loadListFromLocalStorage());

btnClearEl.addEventListener('click', () => {
    clearList()
});

btnAddEl.addEventListener('click', () => {
    addTask();
});

btnBackspace.addEventListener('click', () => {
    const getTaskFromLS = JSON.parse(localStorage.getItem("Record"));
    for (const record of getTaskFromLS){       
        if(record.id === +btnUpdate.value ){ 
            saveUpdateTask(record.task );          
         break;
        } 
   }  
});

btnUpdate.addEventListener('click', () => {
    saveUpdateTask(inputEl.value);
});

document.querySelector('.js-group').addEventListener('click', event => {
    if (event.target.classList.contains('js-checkbox')) doneTask(event);
    if (event.target.classList.contains('js-remove')) removeTask(event); 
    if (event.target.classList.contains('js-item-value')) updateTask(event); 
});

function loadListFromLocalStorage(){
    const getTaskFromLS = JSON.parse(localStorage.getItem("Record"));
    document.querySelector('.list').innerHTML = '';
    document.querySelector('.list').innerHTML = '<ul class = "list-group js-group"></ul>';
    if(localStorage.getItem("Record") !== null){       
        const listEl = document.querySelector('.list-group');
        for (const record of getTaskFromLS){
            listEl.prepend(createLiEl(record.id, record.task, record.state));
        }
    }
}

function createLiEl(id, task, state) {
    const liEl = document.createElement('li');
    liEl.classList.add('list-group-item');
    liEl.classList.add('js-item');
    liEl.setAttribute('value', id);

    const formGroupEl = document.createElement('div');
    formGroupEl.classList.add('form-group');

    const checkboxEl = document.createElement('input');
    checkboxEl.classList.add('form-check-input');
    checkboxEl.classList.add('js-checkbox');
    checkboxEl.setAttribute('type', 'checkbox');
    checkboxEl.setAttribute('value', id);
    state === 'done' ? checkboxEl.cheked = true : checkboxEl.checked = false;

    const spanEl = document.createElement('span');
    spanEl.classList.add('js-item-value');
    spanEl.textContent = task;
    spanEl.classList.add(state);

    const removeEl = document.createElement('button');
    removeEl.classList.add('js-remove', 'btn','mx-2','btn-outline-danger');
    removeEl.textContent = 'X';

    formGroupEl.append(checkboxEl);
    formGroupEl.append(spanEl);

    liEl.append(formGroupEl);
    liEl.append(removeEl);

    // // для новых элементов
    // checkboxEl.addEventListener('click', event => {
    //     const parent = checkboxEl.parentNode;
    //     parent.querySelector('.js-item-value').classList.toggle('done');
    // });

    return liEl;

}

function clearList(){
    window.localStorage.clear();
    document.querySelector('.list-group').innerHTML = '';
    inputEl.value = '';
    btnUpdate.hidden = true;
    btnAddEl.hidden = false;
    inputEl.focus();
}

function addTask(){
    const getTaskFromLS = JSON.parse(localStorage.getItem("Record"));
    if (getTaskFromLS === null) localStorage.setItem("RecordCount", 0);
    let recordCountLS = JSON.parse(localStorage.getItem("RecordCount"));
    const addTask ={
     id: recordCountLS++,
     task: inputEl.value.trim() === '' ? 'Пустая задача' : inputEl.value.trim(),
     state: 'active'
    };
 
    if(getTaskFromLS === null) {
         let arrayTask = [];
         arrayTask.push(addTask);
         localStorage.setItem('Record', JSON.stringify(arrayTask));
         localStorage.setItem('RecordCount', JSON.stringify(recordCountLS++));
         inputEl.value = '';
     }else{
         getTaskFromLS.push(addTask);
         localStorage.setItem('Record', JSON.stringify(getTaskFromLS));
         localStorage.setItem('RecordCount', JSON.stringify(recordCountLS++));
         inputEl.value = '';
     }
     const listEl = document.querySelector('.list-group');
     if(recordCountLS > 1)listEl.prepend(createLiEl(addTask.id,addTask.task,addTask.state));else
         listEl.append(createLiEl(addTask.id,addTask.task,addTask.state));
     inputEl.focus();
}

function doneTask(event){
    const getTaskFromLS = JSON.parse(localStorage.getItem("Record"));
    const parentNode = event.target.parentNode;
        parentNode.querySelector('.js-item-value').classList.toggle('done');
        const idCheckbox = +(event.target.value);
        for (const record of getTaskFromLS){       
            if(record.id === idCheckbox ){ 
                record.state === 'done' ? record.state = 'active' : record.state = 'done';
                localStorage.setItem('Record', JSON.stringify(getTaskFromLS));                
             break;
            }     
       }  
}

function removeTask(event){
    const getTaskFromLS = JSON.parse(localStorage.getItem("Record"));
    const idTask = +(event.target.parentElement.value);
    for (let i = 0; i<getTaskFromLS.length; i++){ 
        if(getTaskFromLS[i].id === idTask) {
            getTaskFromLS.splice(i,1);
            localStorage.setItem('Record', JSON.stringify(getTaskFromLS)); 
            break; 
        }       
    }
    event.target.parentElement.remove();
}

function updateTask(event){
    if(btnUpdate.hidden === true){
        const idTask = (event.target.parentElement.parentElement.value);
        const task = event.target.innerHTML;
        inputEl.value = task;
        event.target.parentElement.parentElement.classList.add('editTask');
        inputEl.select();
        btnUpdate.hidden = false;
        btnAddEl.hidden = true;
        btnUpdate.value = idTask; 
        btnBackspace.hidden = false;
    }
}

function saveUpdateTask(inputTaskValue){
    const getTaskFromLS = JSON.parse(localStorage.getItem("Record"));
    for (const record of getTaskFromLS){       
        if(record.id === +btnUpdate.value ){ 
            record.task = inputTaskValue;
            localStorage.setItem('Record', JSON.stringify(getTaskFromLS));                
         break;
        }                 

   }  
   const listTask = document.querySelector('.list-group').childNodes;
   for (let i = 0; i < listTask.length; i++){ 
        if(listTask[i].value === +btnUpdate.value) {
            listTask[i].childNodes[0].querySelector('.js-item-value').innerHTML = inputTaskValue;
            listTask[i].classList.remove('editTask');
            break; 
        }       
    }
   inputEl.value = '';
   btnUpdate.hidden = true;
   btnAddEl.hidden = false;
   btnBackspace.hidden = true;
};
// const checkboxEl = document.querySelector('.js-checkbox');
//
// checkboxEl.addEventListener('click', event => {
//     const parent = checkboxEl.parentNode;
//     parent.querySelector('.js-item-value').classList.toggle('done');
// });

// для существующих элементов списка на момент загруки стр
// const checkboxList = document.querySelectorAll('.js-checkbox');
//
// for (let i = 0; i < checkboxList.length; i++) {
//     const checkboxItem = checkboxList.item(i);
//     checkboxItem.addEventListener('click', event => {
//         const parent = checkboxItem.parentNode;
//         parent.querySelector('.js-item-value').classList.toggle('done');
//     });
// }
